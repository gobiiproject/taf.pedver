module.exports = {
  globals_path: "globals.js",
  page_objects_path: "./src/page_objects", //page_objects folder where selectors are saved
  selenium: {
    start_process: true,
    host: "127.0.0.1",
    port: 4444,
    server_path: require("selenium-server").path,
    cli_args: {
      "webdriver.gecko.driver": require("geckodriver").path,
      "webdriver.chrome.driver": require("chromedriver").path,
      "webdriver.edge.driver": "./driver/msedgedriver.exe",
    },
  },

  test_settings: {
    default: {
      screenshots: {
        enabled: true,
        path: "./screenshots",
      },
    },

    chrome: {
      desiredCapabilities: {
        browserName: "chrome",
        chromeOptions: {
          args: ["--no-sandbox"],
          w3c: false,
        },
      },
    },
    firefox: {
      desiredCapabilities: {
        browserName: "firefox",
      },
    },
    edge: {
      desiredCapabilities: {
        browserName: "MicrosoftEdge",
        edgeOptions: {
          args: ["--no-sandbox"],
          w3c: false,
        },
      },
    },
    "headless.chrome": {
      extends: "chrome",
      desiredCapabilities: {
        chromeOptions: {
          args: ["--headless", "--disable-gpu", "--window-size=1280,1024"],
          w3c: false,
        },
      },
    },
    "headless.firefox:": {
      extends: "firefox",
      moz: {
        firefoxOptions: {
          args: ["-headless", "-width=1280", "-height=1024"],
        },
      },
    },
    "headless.edge": {
      extends: "edge",
      desiredCapabilities: {
        edgeOptions: {
          args: [
            "--no-sandbox",
            "--headless",
            "--disable-gpu",
            "--window-size=1280,1024",
          ],
          w3c: false,
        },
      },
    },
  },
};
