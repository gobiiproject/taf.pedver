Feature: Study Selection

        Background: Login user
            Given user logs to PedVer
             When user selects Studies to "F1_ped_ver_rice"
              And user selects VariantSets to "F1_ped_ver_rice-F1_ped_ver_rice_missing and 1 rep parents (9 * 85)"
              And user clicks Next button of 1
              And user clicks Next button of 2
              And user clicks Next button of 3

        @debug
        Scenario: User selects a dataset
             When user clicks Next button of 4
              And user clicks Yes button
            #  Then user can download the file

            