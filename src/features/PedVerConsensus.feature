Feature: PedVer Consensus Page
        Background: Login User (While connected to the vpn)
            Given user logs to PedVer
             When user selects Studies to "F1_ped_ver_rice"
              And user selects VariantSets to "F1_ped_ver_rice-F1_ped_ver_rice_missing and 1 rep parents (9 * 85)"
              And user clicks Next button of 1
              And user clicks Next button of 2
              And user sees "majority genotype favoring homozygotes (parents only)" is selected
              And user clicks Apply button of 3
			  
        Scenario: User applies a consensus
             Then user sees a success message
              And user sees the Consensus Process as "CONSENSUS-CALLING-INPROGRESS, CONSENSUS-CALLING-INPROGRESS : consensus calling in progress, DONE"
              And user sees Download button
              And user sees table of Consensus
                  | Name                | snpOS0287 | snpOS0289 | snpOS0290 | snpOS0291 | snpOS0292 | snpOS0296 |
                  | IR04A428*           | T/T       | T/T       | C/C       | G/G       | G/G       | A/A       |
                  | 181GPUR_ICP_1_10_10 | T/T       | T/T       | C/C       | G/G       | G/G       | A/A       |
                  | 181GPUR_ICP_1_11_11 | T/T       | T/T       | C/C       | G/G       | G/G       | A/A       |
                  | 181GPUR_ICP_1_1_1   | T/T       | T/T       | C/C       | G/G       | G/G       | A/A       |
                  | 181GPUR_ICP_1_2_2   | T/T       | T/T       | C/C       | G/G       | G/G       | A/A       |
                  | 181GPUR_ICP_1_3_3   | T/T       | T/T       | C/C       | G/G       | G/G       | A/A       |
                  | 181GPUR_ICP_1_4_4   | T/T       | T/T       | C/C       | G/G       | G/G       | A/A       |
			  
        Scenario: User applies a consensus and downloads data
             Then user sees green message is displayed
			