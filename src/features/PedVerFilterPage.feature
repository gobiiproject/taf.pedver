Feature: PedVer Filter Page
        Background: Login User (While connected to the vpn)
            Given user logs to PedVer
             When user selects VariantSets to "CIMMYT Wheat - marker-assisted selection-CIMMYT Wheat - marker-assisted selection (23 * 622)"
              And user clicks Next button of 1
		
        Scenario:  User enters invalid Marker Percentage
              And user sets Select Marker Percent to "101"
             Then user see "Maximum value is 100" warning of Select Marker Percent
        
        Scenario:  User enters invalid Sample Percentage
              And user sets Select Sample Percent to "101"
             Then user see "Maximum value is 100" warning of Select Sample Percent

        Scenario: User filters variantset by marker and sample
              And user sets Select Marker Percent to "11"
              And user sets Select Sample Percent to "11"
              And user clicks Apply button of 2
             Then user sees a success message
              And user sees the Filter Process as "FILTER-GENOTYPES-COMPLETED, FILTER-GENOTYPES-COMPLETED : genotypes filtering completed, DONE"
              And user sees "22 markers remaining (1 excluded)" of section
              And user sees "622 samples remaining (0 excluded)" of section
              And user sees table of Filter
                  | Name           | 6DS_2105488_5581_kwm907 | Contig11536236_557_kwm999 | Contig11536236_558_kwm1000 | IWA7257 | IWB28643 |
                  | WL18MVSD000262 | A/A                     | T/T                       | A/A                        | T/T     | G/G      |
                  | WL18MVSD000263 | A/A                     | T/T                       | A/A                        | T/T     | G/G      |
                  | WL18MVSD000264 | A/A                     | T/T                       | A/A                        | T/T     | A/A      |
                  | WL18MVSD000265 | A/A                     | T/T                       | A/A                        | T/T     | A/A      |
                  | WL18MVSD000266 | A/A                     | T/T                       | A/A                        | T/T     | G/A      |
                  