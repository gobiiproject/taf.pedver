Feature: Study Selection

        Background: Login user
            Given user logs to PedVer

        Scenario Outline: Studies Selection
             When user selects Studies to "<Study>"
             Then user sees VariantSets selections available are "<Selections>"

        Examples:
                  | Study                                    | Selections                                                                                                                                                   |
                  | F1_ped_ver_rice                          | F1_ped_ver_rice-F1_ped_ver_rice (9 * 106), F1_ped_ver_rice-F1_ped_ver_rice_missing and 1 rep parents (9 * 85), F1_ped_ver_rice-F1_ped_ver_rice_old (9 * 106) |
                  | CIMMYT Wheat - marker-assisted selection | CIMMYT Wheat - marker-assisted selection-CIMMYT Wheat - marker-assisted selection (23 * 622)                                                                 |
                    