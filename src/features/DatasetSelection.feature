Feature: Dataset Selection

        Background: Login user
            Given user logs to PedVer
             When user selects Studies to "F1_ped_ver_rice"
              And user selects VariantSets to "F1_ped_ver_rice-F1_ped_ver_rice_missing and 1 rep parents (9 * 85)"

        Scenario: User selects a dataset
             Then user sees eye button beside the dataset

        Scenario: User selects a dataset and previews it
              And user selects eye button
             Then user sees label "85 Samples, 9 Markers"
              And user sees table
                  | Name             | Variant: snpOS0307 | Variant: snpOS0309 | Variant: snpOS0287 | Variant: snpOS0289 | Variant: snpOS0290 | Variant: snpOS0291 | Variant: snpOS0292 | Variant: snpOS0296 | Variant: snpOS0298 |
                  | A2018DSC5_1_1511 | G/T                | C/C                | T/T                | T/T                | C/C                | G/G                | G/G                | A/G                | A/T                |
                  | A2018DSC5_2_1512 | G/T                | C/C                | T/T                | T/T                | C/C                | G/G                | G/G                | A/G                | A/T                |
                  | A2018DSC5_3_1513 | G/T                | C/C                | T/T                | T/T                | C/C                | G/G                | G/G                | A/G                | A/T                |
                  | A2018DSC5_4_1514 | G/T                | C/C                | T/T                | T/T                | C/C                | G/G                | G/G                | A/G                | A/T                |
                  | A2018DSC5_5_1515 | G/T                | C/C                | T/T                | T/T                | C/C                | G/G                | G/G                | A/G                | A/T                |
                  | A2018DSC5_6_1516 | G/T                | C/C                | T/T                | T/T                | C/C                | G/G                | G/G                | A/G                | A/T                |
                  | A2018DSC5_7_1517 | T/T                | C/C                | T/T                | T/T                | C/C                | G/G                | G/G                | A/A                | A/A                |
                  | A2018DSC5_8_1518 | G/T                | C/C                | T/T                | T/T                | C/C                | G/G                | G/G                | N/N                | A/T                |

        Scenario: User deselects a dataset
              And user removes "F1_ped_ver_rice-F1_ped_ver_rice_missing and 1 rep parents (9 * 85)" from Datasets
             Then user not sees eye button beside the dataset

        Scenario: User deselects a dataset and moves to the next page
             When user removes "F1_ped_ver_rice-F1_ped_ver_rice_missing and 1 rep parents (9 * 85)" from Datasets
              And user clicks Next button of 1
             Then user sees "Please select a dataset" warning
