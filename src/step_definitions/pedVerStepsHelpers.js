const { client } = require("nightwatch-api");
const pedVerPage = client.page.pedVerPage();
//await pedVerPage.perform(()=>{debugger;})

module.exports = {
  login: async function () {
    const url = "http://api.gobii.org:8081/gobii-dev/";
    const user = "gadm";
    const password = "g0b11Admin";
    await pedVerPage
      .navigate()
      .waitForElementVisible("@urlInput")
      .setValue("@urlInput", url)
      .setValue("@userNameInput", user)
      .setValue("@passwordInput", password)
      .assert.visible("@submitButton")
      .click("@submitButton");
  },
};
