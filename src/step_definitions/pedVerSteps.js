const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const pedVerPage = client.page.pedVerPage();
const pedVerStepsHelpers = require("./pedVerStepsHelpers.js");
const chai = require("chai");
var dragAndDrop = require('html-dnd').codeForSelectors;

//await gobii.perform(()=>{debugger;});

Given(
  /^user logs to PedVer$/,
  async () => {
    await pedVerStepsHelpers.login();
  }
);

When(/^user selects Studies to "([^"]*)"$/, async (args1) => {
  await pedVerPage.select("@studiesSelect", args1);
});

When(/^user selects VariantSets to "([^"]*)"$/, async (args1) => {
  await pedVerPage.select("@variantSetsSelect", args1);
});

When(/^user selects Mapsets to "([^"]*)"$/, async (args1) => {
  await pedVerPage.select("@mapsetsSelect", args1);
});

Then(
  /^user sees VariantSets selections available are "([^"]*)"$/,
  async (args1) => {
    await pedVerPage.pause(1000);
    await pedVerPage.click("div[aria-owns='list-91'] i[aria-hidden='true']");
    const datasets = args1.split(",");
    for (const item of datasets) {
      await pedVerPage.assert.visible({
        selector: `//div[text()='${item.trim()}']`,
        locateStrategy: "xpath",
      });
    }
  }
);

Then(/^user sees eye button beside the dataset$/, async () => {
  await pedVerPage.assert.visible("@eyeIcon");
});

When(/^user selects eye button$/, async () => {
  await pedVerPage.click("@eyeIcon");
});

Then(/^user sees label "([^"]*)"$/, async (args1) => {
  let title;
  await pedVerPage.waitForElementVisible("@modalTitle");
  await pedVerPage.getText("@modalTitle", ({ value }) => {
    title = value;
  });

  title = title.replace(/[\n\r]/g, "");

  await pedVerPage.assert.equal(title, args1);
});

When(/^user sees table$/, async (table) => {
  for (column of table.raw()[0]) {
    for (i = 0; i < table.hashes().length; i++) {
      switch (column) {
        case "Name":
          await pedVerPage.expect
            .element(`#inspire tr:nth-child(${i + 1}) td.text-start`)
            .text.equal(table.hashes()[i][column]);
          break;
        case "Variant: snpOS0307":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(2)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Variant: snpOS0309":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(3)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Variant: snpOS0287":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(4)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Variant: snpOS0289":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(5)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Variant: snpOS0290":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(6)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Variant: snpOS0291":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(7)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Variant: snpOS0292":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(8)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Variant: snpOS0296":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(9)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Variant: snpOS0298":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(10)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: none, Position: none, Variant: snpOS0307":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(2)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: none, Position: none, Variant: snpOS0309":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(3)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: none, Position: none, Variant: snpOS0287":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(4)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: none, Position: none, Variant: snpOS0289":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(5)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: none, Position: none, Variant: snpOS0290":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(6)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: none, Position: none, Variant: snpOS0291":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(7)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: none, Position: none, Variant: snpOS0292":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(8)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: none, Position: none, Variant: snpOS0296":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(9)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: none, Position: none, Variant: snpOS0298":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(10)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: 1, Position: 134, Variant: Sr26_R-dom_A":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(2)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: 1, Position: 40, Variant: PMP3-2":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(3)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: 1, Position: 70, Variant: Lr68-2":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(4)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: 1, Position: 90, Variant: Lr67_TM4":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(5)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: 1, Position: 100, Variant: Yr15-R5":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(6)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: 1, Position: 130, Variant: Sr22_A_AL_Sus-T":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(7)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: 1, Position: 10, Variant: Lr34_TCCIND":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(8)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: 1, Position: 20, Variant: Sr2_ger9 3p":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(9)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "LG Name: 1, Position: 80, Variant: VPM_SNP":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(10)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "6DS_2105488_5581_kwm907":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(2)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Contig11536236_557_kwm999":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(3)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Contig11536236_558_kwm1000":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(4)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "IWA7257":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(5)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "IWB28643":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-center:nth-of-type(6)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        default:
          throw new Error(`Unexpected ${column} name.`);
      }
    }
  }
});

When(/^user sees table of Filter$/, async (table) => {
  for (column of table.raw()[0]) {
    for (i = 0; i < table.hashes().length; i++) {
      switch (column) {
        case "Name":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(1)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "6DS_2105488_5581_kwm907":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(2)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Contig11536236_557_kwm999":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(3)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "Contig11536236_558_kwm1000":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(4)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "IWA7257":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(5)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "IWB28643":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(6)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0287":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(2)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0289":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(3)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0290":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(4)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0291":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(5)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0292":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(6)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0296":
          await pedVerPage.expect
            .element(
              `#inspire tr:nth-child(${i + 1}) td.text-start:nth-of-type(7)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        default:
          throw new Error(`Unexpected ${column} name.`);
      }
    }
  }
});

When(/^user sees table of Consensus$/, async (table) => {
  for (column of table.raw()[0]) {
    for (i = 0; i < table.hashes().length; i++) {
      switch (column) {
        case "Name":
          await pedVerPage.expect
            .element(
              `#inspire div[apicallfunctionname='getConsensusDataPreview'] tr:nth-child(${
                i + 1
              }) td:nth-child(1)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0287":
          await pedVerPage.expect
            .element(
              `#inspire div[apicallfunctionname='getConsensusDataPreview'] tr:nth-child(${
                i + 1
              }) td:nth-child(2)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0289":
          await pedVerPage.expect
            .element(
              `#inspire div[apicallfunctionname='getConsensusDataPreview'] tr:nth-child(${
                i + 1
              }) td:nth-child(3)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0290":
          await pedVerPage.expect
            .element(
              `#inspire div[apicallfunctionname='getConsensusDataPreview'] tr:nth-child(${
                i + 1
              }) td:nth-child(4)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0291":
          await pedVerPage.expect
            .element(
              `#inspire div[apicallfunctionname='getConsensusDataPreview'] tr:nth-child(${
                i + 1
              }) td:nth-child(5)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0292":
          await pedVerPage.expect
            .element(
              `#inspire div[apicallfunctionname='getConsensusDataPreview'] tr:nth-child(${
                i + 1
              }) td:nth-child(6)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        case "snpOS0296":
          await pedVerPage.expect
            .element(
              `#inspire div[apicallfunctionname='getConsensusDataPreview'] tr:nth-child(${
                i + 1
              }) td:nth-child(7)`
            )
            .text.equal(table.hashes()[i][column]);
          break;
        default:
          throw new Error(`Unexpected ${column} name.`);
      }
    }
  }
});

When(/^user removes "([^"]*)" from Datasets$/, async (args1) => {
  const sel = {
    selector: `//span[text()='${args1}'][@class='v-chip__content']/parent::span//i`,
    locateStrategy: "xpath",
  };
  await pedVerPage.click(sel);
});

Then(/^user not sees eye button beside the dataset$/, async () => {
  await pedVerPage.expect.element("@eyeIcon").not.present.after(2000);
});

Then(/^user sees "([^"]*)" warning$/, async (args1) => {
  await pedVerPage.expect.element("@alertContainer").text.equals(args1);
});

When(/^user clicks Next button of 1$/, async () => {
  await pedVerPage.waitForElementVisible("@next1Button");
  await pedVerPage.click("@next1Button");
});

When(/^user clicks Next button of 2$/, async () => {
  await pedVerPage.waitForElementVisible("@next2Button");
  await pedVerPage.click("@next2Button");
});

When(/^user clicks Next button of 3$/, async () => {
  await pedVerPage.waitForElementVisible("@next3Button");
  await pedVerPage.click("@next3Button");
});

When(/^user clicks Next button of 4$/, async() => {
  await pedVerPage.waitForElementVisible("@next4Button");
  await pedVerPage.click("@next4Button");
});


When(/^user sets Select Marker Percent to "([^"]*)"$/, async (args1) => {
  await pedVerPage.setValue("@selectMarkerPercent", args1);
});

Then(/^user see "([^"]*)" warning of Select Marker Percent$/, async (args1) => {
  await pedVerPage.expect
    .element("@selectMarkerPercentWarning")
    .text.equal(args1);
});

When(/^user sets Select Sample Percent to "([^"]*)"$/, async (args1) => {
  await pedVerPage.setValue("@selectSamplePercent", args1);
});

Then(/^user see "([^"]*)" warning of Select Sample Percent$/, async (args1) => {
  await pedVerPage.expect
    .element("@selectSamplePercentWarning")
    .text.equal(args1);
});

When(/^user clicks Apply button of 2$/, async () => {
  await pedVerPage.click("@apply2Button");
});

When(/^user clicks Apply button of 3$/, async () => {
  await pedVerPage.waitForElementVisible("@apply3Button");
  await pedVerPage.click("@apply3Button");
});

Then(/^user sees a success message$/, async () => {
  await pedVerPage.assert.visible("@successfulText");
});

Then(/^user sees the Filter Process as "([^"]*)"$/, async (args1) => {
  let process = [];

  await client.elements(
    "xpath",
    "//div[@class='v-timeline-item__body']",
    function ({ value }) {
      for (const element in value) {
        client.elementIdText(value[element].ELEMENT, function ({ value }) {
          process.push(value);
        });
      }
    }
  );
  const processList = args1.split(",").map((i) => i.trim());
  processList.forEach(async (element) => {
    chai.assert.isOk(process.includes(element));
  });
});

When(/^user sees "([^"]*)" is selected$/, async (args1) => {
  switch (args1) {
    case "majority genotype":
      await pedVerPage.assert.attributeContains(
        "@majorityGenotypeButton",
        "checked",
        "true"
      );
      break;
    case "majority genotype favoring homozygotes (parents only)":
      await pedVerPage.assert.attributeContains(
        "@majorityGenotypeFavoringHomoZygotesButton",
        "checked",
        "true"
      );
      break;
    case "majority genotype, homozygous parent, and similarity imputation":
      await pedVerPage.assert.attributeContains(
        "@majorityGenotypeHomozygousParentSimilarityImputationButton",
        "checked",
        "true"
      );
      break;
    case "majority genotype, homozygous parent, and similarity imputation with recursion":
      await pedVerPage.assert.attributeContains(
        "@majorityGenotypeHomozygousParentSimilarityImputationRecursionButton",
        "checked",
        "true"
      );
      break;
    default:
      throw new Error(`Unexpected ${args1} name.`);
  }
});

Then(/^user sees the Consensus Process as "([^"]*)"$/, async (args1) => {
  let process = [];

  await client.elements(
    "xpath",
    "//div[@class='v-timeline-item__body']",
    function ({ value }) {
      for (const element in value) {
        client.elementIdText(value[element].ELEMENT, function ({ value }) {
          process.push(value);
        });
      }
    }
  );
  const processList = args1.split(",").map((i) => i.trim());
  processList.forEach(async (element) => {
    chai.assert.isOk(process.includes(element));
  });
});

Then(/^user sees Download button$/, async () => {
  await pedVerPage.assert.visible("@downloadButton");
});

Then(/^user sees "([^"]*)" of section$/, async (args1) => {
  let textContent;

  await pedVerPage.getText(
    "#inspire div.v-stepper__items div:nth-child(2) div.row.row--dense div.v-list-item.theme--light",
    ({ value }) => {
      textContent = value;
    }
  );

  await pedVerPage.perform(() => {
    debugger;
  });

  chai.assert.isOk(
    textContent.replace(/(\r\n|\n|\r|\s+)/gm, " ").includes(args1),
    args1 + " not found."
  );
});

Then(/^user sees green message is displayed$/, async () => {
  const selector =
    "#inspire > div > div > div.v-stepper__items > div:nth-child(3) > div > div > div > div:nth-child(1) > div.col.col-3 > div > div.v-card__text.mt-50.success--text.darken-1.font-weight-bold.overline";
    await pedVerPage.waitForElementVisible(selector);
  await pedVerPage.assert.containsText(
    selector,
    "*THIS IS ONLY A PREVIEW OF THOSE THAT WAS CONSENSUS CALLED. PLEASE DOWNLOAD TO SEE THE REST OF THE DATA.".replace(
      /(\r\n|\n|\r|\s+)/gm,
      " "
    )
  );

  // await pedVerPage.waitForElementVisible(selector);
  // await pedVerPage.getText(selector, (result)=>{
  //   console.log("value here=========" + result);
  //   debugger;
  // })
});


// When(/^user moves "([^"]*)" to Selected Split Columns$/, async(args1) => {
//   await client.pause(5000);
  // const element = {selector:"//div[text()='germplasm_type']/../..", locateStrategy:"xpath"};
  // await client.waitForElementVisible("#inspire > div > div > div.v-stepper__items > div:nth-child(4) > div > div > div > div > div:nth-child(1) > div > div:nth-child(2) > div:nth-child(7) > div.v-list-item__content > div");
  // await client.moveToElement("#inspire > div > div > div.v-stepper__items > div:nth-child(4) > div > div > div > div > div:nth-child(1) > div > div:nth-child(2) > div:nth-child(7)", 1,1);
  // await client.pause(1000);
  // await client.mouseButtonDown(0);
  // await client.pause(1000);
  // const dest = {selector:"//div[text()='SELECTED SPLIT COLUMNS:']/parent::div/div[2]", locateStrategy:"xpath"};
  // await client.moveToElement(dest, 1,1);
  // await client.mouseButtonUp(0);

  // const elemSrc = "//div[text()='germplasm_type']/../..";
//   const elemSrc = "#inspire > div > div > div.v-stepper__items > div:nth-child(4) > div > div > div > div > div:nth-child(1) > div > div:nth-child(2) > div:nth-child(7)";
//   const elemDst = "//div[text()='SELECTED SPLIT COLUMNS:']/parent::div/div[2]";

//   await client.execute(dragAndDrop, [elemSrc, elemDst]);

// });


When(/^user clicks Yes button$/, async() => {
	await pedVerPage.waitForElementVisible("@yesButton");
	await pedVerPage.click("@yesButton");
});
