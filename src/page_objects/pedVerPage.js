const commands = require("./pedVerCommands");

module.exports = {
  url: "http://cbsugobiixvm10.biohpc.cornell.edu:7000/#/",
  commands: [commands.commands],
  elements: {
    //login
    urlInput: "#input-24",
    userNameInput: "#input-28",
    passwordInput: "#password",
    submitButton: ".v-btn__content",

    studiesSelect: {
      selector: "//input[@id='input-76']/../following-sibling::div//i",
      locateStrategy: "xpath",
    },
    variantSetsSelect: {
      selector: "//input[@id='input-91']/../following-sibling::div//i",
      locateStrategy: "xpath",
    },
    mapsetsSelect: {
      selector: "//input[@id='input-106']/../following-sibling::div//i",
      locateStrategy: "xpath",
    },

    eyeIcon: "div button span i.v-icon.notranslate.mdi.mdi-eye.theme--light",

    modalTitle:
      "#inspire div.v-dialog__content.v-dialog__content--active div.v-toolbar__title",
    next1Button:
      "#inspire div.v-stepper__items > div:nth-child(1) > div > button > span > i",
    next2Button:
      "#inspire > div > div > div.v-stepper__items > div:nth-child(2) > div > button.v-btn.v-btn--bottom.v-btn--contained.v-btn--fab.v-btn--fixed.v-btn--right.v-btn--round.theme--dark.v-size--default.primary > span > i",

    next3Button:
      "#inspire > div > div > div.v-stepper__items > div:nth-child(3) > div > button.v-btn.v-btn--bottom.v-btn--contained.v-btn--fab.v-btn--fixed.v-btn--right.v-btn--round.theme--dark.v-size--default.primary > span > i",

    next4Button:
      "#inspire > div > div > div.v-stepper__items > div:nth-child(4) > div > button.v-btn.v-btn--bottom.v-btn--contained.v-btn--fab.v-btn--fixed.v-btn--right.v-btn--round.theme--dark.v-size--default.primary > span > i",

    alertContainer: "div.v-alert__content",

    selectMarkerPercent: "#input-122",
    selectSamplePercent: "#input-130",
    selectMarkerPercentWarning:
      "#inspire > div > div > div.v-stepper__items > div:nth-child(2) > div > div > div > div.row.row--dense > div.col-sm-5.col-md-4.col-6 > form > div:nth-child(1) > div.col-sm-8.col-md-6.col-lg-5.col-8 > div > div.v-input__control > div.v-text-field__details > div > div > div",
    selectSamplePercentWarning:
      "#inspire > div > div > div.v-stepper__items > div:nth-child(2) > div > div > div > div.row.row--dense > div.col-sm-5.col-md-4.col-6 > form > div:nth-child(2) > div.col-sm-8.col-md-6.col-lg-5.col-8 > div > div.v-input__control > div.v-text-field__details > div > div > div",

    apply2Button: {
      selector:
        "//button[contains(@class,'ma-2 v-btn v-btn--depressed v-btn--flat v-btn--outlined theme--light v-size--default accent--text')]/span[text()='apply']",
      locateStrategy: "xpath",
    },

    apply3Button: {
      selector:
        "//button[contains(@class,'ma-1 v-btn v-btn--depressed v-btn--flat v-btn--outlined theme--light v-size--default accent--text')]/span[text()='apply']",
      locateStrategy: "xpath",
      index: 0,
    },

    successfulText: {
      selector: "//strong[text()='Successful!']",
      locateStrategy: "xpath",
    },

    majorityGenotype: "#input-146",
    majorityGenotypeFavoringHomoZygotesButton: "#input-148",
    majorityGenotypeHomozygousParentSimilarityImputationButton: "#input-152",
    majorityGenotypeHomozygousParentSimilarityImputationRecursionButton:
      "#input-154",

    downloadButton:
      "#inspire > div > div > div.v-stepper__items > div:nth-child(3) > div > div > div > div:nth-child(1) > div.col.col-3 > div > div.v-card__actions > button > span",

    yesButton:
      "#inspire div.v-dialog__content div div.v-card__actions button:nth-child(4)",
  },
};
