module.exports = {
  commands: {
    select: function (control, option){
      this.waitForElementVisible(control);
      this.click(control);
      const elem = {
        selector: `//div[@role='listbox']//div[text()='${option}']`,
        locateStrategy: "xpath",};
      this.waitForElementVisible(elem);
      this.moveToElement(elem, 5,5);
      this.click(elem);
      return this.pause(1000);
    },
  },
};
