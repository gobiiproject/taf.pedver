const fs = require("fs");
const path = require("path");
const { setDefaultTimeout, Before, After, AfterAll, BeforeAll } = require("cucumber");
const {
  createSession,
  closeSession,
  startWebDriver,
  stopWebDriver, getNewScreenshots
} = require("nightwatch-api");
const reporter = require("cucumber-html-reporter");
const { client } = require("nightwatch-api");

const attachedScreenshots = getScreenshots();

function getScreenshots() {
  try {
    const folder = path.resolve(__dirname, "screenshots");
    const screenshots = fs
      .readdirSync(folder)
      .map((file) => path.resolve(folder, file));
    return screenshots;
  } catch (err) {
    return [];
  }
}

setDefaultTimeout(120000);

BeforeAll(async () => {
  await startWebDriver({
    env: process.argv[process.argv.indexOf("--env") + 1],
  });
 
});

AfterAll(async () => {
  await stopWebDriver();

  setTimeout(() => {
    reporter.generate({
      theme: "bootstrap",
      jsonFile: "report/cucumber_report.json",
      output: "report/cucumber_report.html",
      reportSuiteAsScenarios: true,
      launchReport: true,
      metadata: {
        // OS: platform,
        // Browser: browserName,
        // Version: version,
        // "GOBii Extractor Version": gobiiVersion,
      },
    });
  }, 0);
});

Before(async function(){
  await createSession();
  await client.maximizeWindow();
});

After(async function () {
  getNewScreenshots().forEach(file => this.attach(fs.readFileSync(file), 'image/png'));
  // await closeSession();
});
